<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test Suite untuk fitur login</description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>80</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>db18a0c4-9448-4bfd-8278-f92161c963f5</testSuiteGuid>
   <testCaseLink>
      <guid>9557bbf1-cf56-4a81-8f21-b6ed2c079b1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 01 - Verifikasi fitur login/TCLog 01 - Verifikasi halaman sign in</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f66c3096-a5c6-43a7-a1ee-db57c2f039c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/POSITIVE/TCLog 02 - User menginput semua data dengan valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0217a4a9-dcf4-4dec-b909-9fba46ea1dbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/NEGATIVE/TCLog 04 - User menginput email dan password null</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2bd2afb6-d5c0-4eef-960f-9b8cb5985f02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/NEGATIVE/TCLog 05 - User menginput email valid dan password invalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fdd4c4ce-ba86-4b4c-88bf-ae6b3ff22fdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/NEGATIVE/TCLog 06 - User menginput email valid dan password null</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7e8a0335-c7c1-4986-8e8e-4b8002d8468a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/NEGATIVE/TCLog 07 - User menginput email invalid dan password valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9b5b17f1-e318-4c96-a54f-1e14e59fc1c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/NEGATIVE/TCLog 08 - User menginput email null dan password valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e1ebbddb-fd6a-4eac-a660-5bf4f161208b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/POSITIVE/TCLog 09 - User masuk akun lewat google</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b3ba2940-86e0-4859-9600-1f4b6c3b174e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/POSITIVE/TCLog 10 - User masuk akun lewat apple id</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>239a4405-4279-4859-aa17-d3398d9aaeff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 02 - User masuk ke halaman akun/POSITIVE/TCLog 11 - User mendaftar akun melalui link register now</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>de68a97b-c16e-4f22-8bec-ce1d2ddc60f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 03 - User lupa password/POSITIVE/TCLog 12 - Verifikasi halaman lupa password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7b1ceb13-1b91-4410-aff8-59b0c0e427d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 03 - User lupa password/POSITIVE/TCLog 13 - User menginput email valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b2888ff7-0681-4f3c-8508-674c571576b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 03 - User lupa password/NEGATIVE/TCLog 14 - User input email belum terdaftar</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a7c5d8f7-0ff9-4742-9808-9d37335d1b5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/LOGIN/TSLog 03 - User lupa password/NEGATIVE/TCLog 15 - User input email belum terverifikasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
