<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Registrasi</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>80</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e37a5c1f-4cf9-418d-84c0-7802e317ab2b</testSuiteGuid>
   <testCaseLink>
      <guid>28160b11-b1b2-421a-a9cf-9fdcd3497a58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/POSITIVE/TCMR 01 - Registrasi Mengisi semua field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bc0120f5-5aae-4de2-8f5b-cc2c443caa0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/NEGATIVE/TCMR 04 -Mengosongkan Nama</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2ace6ed6-5639-4ba1-b2a2-15234e3010b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/NEGATIVE/TCMR 05 - Mengosongkan Tanggal lahir</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>48e3a4ac-0694-4627-97c0-149cfa0fd5d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/NEGATIVE/TCMR 06 -Mengosongkan E-mail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c859b7e0-623c-48ed-bead-a2438fdbc380</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/NEGATIVE/TCMR 07 Mengosongkan Whatsapp</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5aafe041-54c9-43b3-97d6-627c10c848dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/NEGATIVE/TCMR 08 - Mengosongkan Kata sandi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>41ec8551-85d4-4cb2-9296-c1d1db517517</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BOBY/REGISTRASI/NEGATIVE/TCMR 10 - Tidak ceklis I agree with Term and Condition</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
