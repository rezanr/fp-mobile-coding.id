import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\LENOVO\\Desktop\\B#6\\New APK\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/REZA/LOGIN/TSLog 03 - User lupa password/TCLog 16 - input email belum terverifikasi/android.widget.TextView - Login Here (1)'), 
    0)

Mobile.tap(findTestObject('Object Repository/REZA/LOGIN/TSLog 03 - User lupa password/TCLog 16 - input email belum terverifikasi/android.widget.TextView - Forgot Password (1)'), 
    0)

Mobile.tap(findTestObject('Object Repository/REZA/LOGIN/TSLog 03 - User lupa password/TCLog 16 - input email belum terverifikasi/android.widget.EditText (1)'), 
    0)

Mobile.setText(findTestObject('Object Repository/REZA/LOGIN/TSLog 03 - User lupa password/TCLog 16 - input email belum terverifikasi/android.widget.EditText - Input Email (1)'), 
    'arum@gmail.com', 0)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.takeScreenshot('C:\\Users\\LENOVO\\Documents\\FINAL TEST\\MOBILE\\FP Mobile QA6\\Screenshoot\\screenshot copy(27).png')

Mobile.tap(findTestObject('REZA/LOGIN/TSLog 03 - User lupa password/TCLog 15 - input email belum terdaftar/android.widget.TextView - Request for link reset password to your email'), 
    0)

Mobile.tap(findTestObject('Object Repository/REZA/LOGIN/TSLog 03 - User lupa password/TCLog 16 - input email belum terverifikasi/android.widget.TextView - Send (1)'), 
    0)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.takeScreenshot('C:\\Users\\LENOVO\\Documents\\FINAL TEST\\MOBILE\\FP Mobile QA6\\Screenshoot\\screenshot copy(28).png')

Mobile.closeApplication()

